package com.example.gelala.mobilerestaurant;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class FoodInput extends AppCompatActivity {
    EditText f_nam;
    EditText f_des;
    EditText f_pri;
    EditText f_qty;
    Intent in;
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_input);
        f_nam = (EditText) findViewById(R.id.name);
        f_des = (EditText) findViewById(R.id.desc);
        f_pri = (EditText) findViewById(R.id.price);
        f_qty = (EditText) findViewById(R.id.qty);
        in = new Intent(FoodInput.this, FoodList.class);
        submit = (Button) findViewById(R.id.subbtn);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((f_nam != null) && (f_des != null) && (f_pri != null) && (f_qty != null)) {
                    Toast.makeText(getApplicationContext(), "Added Data Successfully.", Toast.LENGTH_LONG).show();
                    in.putExtra("name", f_nam.getText().toString());
                    in.putExtra("desc", f_des.getText().toString());
                    in.putExtra("prce", f_pri.getText());
                    in.putExtra("qnty", f_pri.getText());
                    startActivity(in);
                }
                else {
                    Toast.makeText(getApplicationContext(), "Please enter name, description, price and quantity.", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_food_input, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}