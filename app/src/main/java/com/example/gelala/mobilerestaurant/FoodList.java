package com.example.gelala.mobilerestaurant;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class FoodList extends AppCompatActivity {
    ListView view_name;
    Intent intt;
    Integer qnt;
    String name, descr;
    Float pric;
    Button addbtn;
    SimpleAdapter mSchedule;
    Bundle fd;
    ArrayList<HashMap<String, String>> mylist;
    HashMap<String, String> map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_list);

        fd = getIntent().getExtras();
        view_name = (ListView) findViewById(R.id.foodview);

        name = (String) fd.getString("name");
        descr = (String) fd.getString("desc");
        pric = (Float) fd.getFloat("prce");
        qnt = (Integer) fd.getInt("qnty");

        mylist = new ArrayList<HashMap<String, String>>();
        map = new HashMap<String, String>();
        map.put("fona", name);
        map.put("fode", descr);
        map.put("fopr", pric.toString());
        map.put("foqu", qnt.toString());
        mylist.add(map);

        mSchedule = new SimpleAdapter(this, mylist, R.layout.activity_list,
                new String[]{"fona", "fode", "fopr", "foqu"}, new int[]{R.id.fn, R.id.fd, R.id.fp, R.id.fq});

        view_name.setAdapter(mSchedule);
        mSchedule.notifyDataSetChanged();

        addbtn = (Button) findViewById(R.id.add);
        addbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intt = new Intent(FoodList.this, FoodInput.class);
                startActivity(intt);
            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_food_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
